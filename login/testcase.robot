*** Settings ***
Resource   ../import.robot
Resource   ./elements.robot
Resource   ./keyword.robot

*** Test Cases ***
Tc1 - login succesful
       Open Viettel Pay
       Common - Click element        ${btn_cancel_id}
       Common - input text           ${inp_sdt_id}         0827931999
       click element                 ${btn_continue_id}
       Common - input text           ${inp_pass_id}         170900

       wait until keyword succeeds     30s     3s      wait until element is visible       Xpath=//android.widget.TextView[contains(@text,"VTPAY")]
       ${otp}=  Common - GetOTP in mess     Xpath=//android.widget.TextView[contains(@text,"VTPAY")]/following::android.widget.TextView[2]
       click element        //android.widget.ImageView
       sleep     3
       input text          com.bplus.vtpay:id/inputOtp      ${otp}

       click element       com.bplus.vtpay:id/buttonPositive
