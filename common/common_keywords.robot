*** Settings ***
Resource     ../import.robot

Library       AppiumLibrary

*** Keywords ***
Open Viettel Pay
        open application
        ...     ${REMOTE_URL}
        ...     platformName=${PLATFORM_NAME}
        ...     platformVersion=${PLATFORM_VERSION}
        ...     deviceName=${DEVICE_NAME}
        ...     appPackage=com.bplus.vtpay
        ...     udid=${udid}
        ...     appActivity=com.bplus.vtpay.activity.SplashScreen
        #...     app=${app}
Common - Click element
        [Arguments]         ${element_lct}
        Wait Until Keyword Succeeds         20s         2s          Element Should Be Visible          ${element_lct}
        Click Element       ${element_lct}
Common - input text
        [Arguments]         ${element_lct}          ${text}
        Wait Until Keyword Succeeds         20s         2s          Element Should Be Visible          ${element_lct}
        input text           ${element_lct}          ${text}


Common - GetOTP in mess
        [Arguments]     ${mess_content}
        ${text}      get text           ${mess_content}
        ${otp}       get otp            ${text}
        [Return]         ${otp}